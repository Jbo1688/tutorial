-- phpMyAdmin SQL Dump
-- version 4.4.15.10
-- https://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 2020-12-29 23:32:48
-- 服务器版本： 5.6.48-log
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tutorial`
--

-- --------------------------------------------------------

--
-- 表的结构 `tutorials`
--

CREATE TABLE IF NOT EXISTS `tutorials` (
  `id` varchar(40) COLLATE utf8mb4_bin NOT NULL,
  `pid` varchar(40) COLLATE utf8mb4_bin DEFAULT '',
  `tid` varchar(40) COLLATE utf8mb4_bin NOT NULL COMMENT '分类编号',
  `title` varchar(255) COLLATE utf8mb4_bin NOT NULL COMMENT '教程标题',
  `rank` int(11) DEFAULT '0' COMMENT '排序编号，数字越大越靠前，默认 0',
  `description` varchar(255) COLLATE utf8mb4_bin DEFAULT '' COMMENT '描述信息，用于seo',
  `keyword` varchar(255) COLLATE utf8mb4_bin DEFAULT '' COMMENT '关键词，用于seo',
  `content` text COLLATE utf8mb4_bin NOT NULL COMMENT '教程内容',
  `hidden` tinyint(1) DEFAULT '1' COMMENT '是否隐藏，true隐藏',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='教程表';

--
-- 转存表中的数据 `tutorials`
--

INSERT INTO `tutorials` (`id`, `pid`, `tid`, `title`, `rank`, `description`, `keyword`, `content`, `hidden`, `created_at`, `updated_at`) VALUES
('12dad2d3b5764e18a11d7ca3b2e62bae', '', '2854d469f1e24e66a19b6a2eaedf2733', 'http协议简介', 0, '', '', '## HTTP协议简介\r\n超文本传输协议（英文：HyperText Transfer Protocol，缩写：HTTP）是一种用于分布式、协作式和超媒体信息系统的应用层协议。HTTP是万维网的数据通信的基础。\r\n\r\nHTTP的发展是由蒂姆·伯纳斯-李于1989年在欧洲核子研究组织（CERN）所发起。HTTP的标准制定由万维网协会（World Wide Web Consortium，W3C）和互联网工程任务组（Internet Engineering Task Force，IETF）进行协调，最终发布了一系列的RFC，其中最著名的是1999年6月公布的 RFC 2616，定义了HTTP协议中现今广泛使用的一个版本——HTTP 1.1。\r\n\r\n2014年12月，互联网工程任务组（IETF）的Hypertext Transfer Protocol Bis（httpbis）工作小组将HTTP/2标准提议递交至IESG进行讨论，于2015年2月17日被批准。 HTTP/2标准于2015年5月以RFC 7540正式发表，取代HTTP 1.1成为HTTP的实现标准。', 0, '2020-12-27 17:20:29', '2020-12-27 17:20:29'),
('7021ae109c24454b95f719e9492b6576', '97b756eca65c4667b4a9f902af47241c', '8f3bcabf732c47bf98f840c2fb135bc6', 'if语句', 10, 'if语句if语句if语句if语句if语句', 'if语句if语句', '\r\n### if语句\r\n\r\n表头|表头|表头|表头\r\n-|-|-|-\r\n表头|表头|表头|表头\r\n表头|表头|表头|表头\r\n表头|表头|表头|表头\r\n表头|表头|表头|表头\r\n表头|表头|表头|表头\r\n表头|表头|表头|表头\r\n', 0, '2020-12-28 06:34:34', '2020-12-28 06:34:34'),
('7b30bbeafeac4844aab26910b09ad8f5', 'a7e45e1c0cdf4f1aa2e204d8ed71e9e5', '8f3bcabf732c47bf98f840c2fb135bc6', 'c语言数组', 5, 'c语言数组c语言数组c语言数组', 'c语言数组c语言数组', '本章开始，我们学习数组。数组可以说是目前为止讲到的第一个真正意义上存储数据的结构。虽然前面学习的变量也能存储数据，但变量所能存储的数据毕竟有限。\r\n\r\n数组本身很重要，每个学习 C 语言的初学者都必须掌握！与此同时，它和指针相辅相成，学习数组可以为学习指针打下良好的基础', 0, '2020-12-29 15:09:27', '2020-12-29 15:09:27'),
('97b756eca65c4667b4a9f902af47241c', '', '8f3bcabf732c47bf98f840c2fb135bc6', '基础语法', 0, '踩踩踩踩踩踩踩踩踩踩踩踩踩踩踩踩踩踩踩踩', '啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊', '## 讲解c语言语法\r\n\r\n![](https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png)', 0, '2020-12-28 05:24:24', '2020-12-28 05:24:24'),
('a369cadbb5c043278ee884b1cbfd96f8', '', '7fe3e46246e04c6f87fe41441471c571', 'rust代码高亮显示', 10, '', '', '# rust代码\r\n```\r\nuse crate::models::{NewType, Type};\r\nuse crate::schema::types::dsl::*;\r\nuse crate::utils;\r\nuse diesel::prelude::*;\r\nuse diesel::MysqlConnection;\r\n\r\npub fn insert(\r\n    conn: &MysqlConnection,\r\n    mut new_type: NewType,\r\n) -> std::result::Result<usize, diesel::result::Error> {\r\n    new_type.id = utils::uuid_str();\r\n    let result = diesel::insert_into(types).values(&new_type).execute(conn)?;\r\n    Ok(result)\r\n}\r\n\r\npub fn update(\r\n    conn: &MysqlConnection,\r\n    new_type: NewType,\r\n) -> std::result::Result<usize, diesel::result::Error> {\r\n    let result = diesel::update(types.filter(id.eq(new_type.id)))\r\n        .set((\r\n            pid.eq(new_type.pid),\r\n            name.eq(new_type.name),\r\n            rank.eq(new_type.rank),\r\n            description.eq(new_type.description),\r\n            keyword.eq(new_type.keyword),\r\n            icon.eq(new_type.icon),\r\n            pic.eq(new_type.pic),\r\n            hidden.eq(new_type.hidden),\r\n        ))\r\n        .execute(conn)?;\r\n    Ok(result)\r\n}\r\n\r\npub fn get_all(\r\n    show_hidden: bool,\r\n    conn: &MysqlConnection,\r\n) -> std::result::Result<Vec<Type>, diesel::result::Error> {\r\n    if show_hidden {\r\n        return types.order(rank.desc()).load::<Type>(conn);\r\n    }\r\n    types\r\n        .filter(hidden.eq(show_hidden))\r\n        .order(rank.desc())\r\n        .load::<Type>(conn)\r\n}\r\n\r\n//根据 id 获取属于该 id 的所有子分类\r\npub fn get_all_children(\r\n    id_str: String,\r\n    show_hidden: bool,\r\n    conn: &MysqlConnection,\r\n) -> std::result::Result<Vec<Type>, diesel::result::Error> {\r\n    if show_hidden {\r\n        types\r\n        .filter(pid.eq(id_str))\r\n        .order(rank.desc())\r\n        .load::<Type>(conn)\r\n    } else {\r\n        types\r\n            .filter(hidden.eq(show_hidden))\r\n            .filter(pid.eq(id_str))\r\n            .order(rank.desc())\r\n            .load::<Type>(conn)\r\n    }\r\n}\r\n\r\npub fn get(\r\n    id_str: String,\r\n    conn: &MysqlConnection,\r\n) -> std::result::Result<Type, diesel::result::Error> {\r\n    types.filter(id.eq(id_str)).first::<Type>(conn)\r\n}\r\n\r\npub fn delete(\r\n    id_str: String,\r\n    conn: &MysqlConnection,\r\n) -> std::result::Result<usize, diesel::result::Error> {\r\n    diesel::delete(types).filter(id.eq(id_str)).execute(conn)\r\n}\r\n\r\n```', 0, '2020-12-28 12:05:01', '2020-12-28 12:05:01'),
('a7e45e1c0cdf4f1aa2e204d8ed71e9e5', '', '8f3bcabf732c47bf98f840c2fb135bc6', '认识c语言', 10, '', '', 'C语言是一门面向过程的、抽象化的通用程序设计语言，广泛应用于底层开发。C语言能以简易的方式编译、处理低级存储器。C语言是仅产生少量的机器语言以及不需要任何运行环境支持便能运行的高效率程序设计语言。尽管C语言提供了许多低级处理的功能，但仍然保持着跨平台的特性，以一个标准规格写出的C语言程序可在包括类似嵌入式处理器以及超级计算机等作业平台的许多计算机平台上进行编译。', 0, '2020-12-28 02:02:10', '2020-12-28 02:02:10'),
('b5712a37e33a485194ef52d00fbb99e0', 'a369cadbb5c043278ee884b1cbfd96f8', '7fe3e46246e04c6f87fe41441471c571', '很多代码的高亮显示', 0, '很多描述', '很多关键字', '# 很多代码\r\n\r\n```\r\nuse actix_files as afs;\r\nuse actix_web::{middleware, web, App, HttpServer,dev::ServiceRequest, Error};\r\nuse diesel::r2d2::ConnectionManager;\r\nuse diesel::MysqlConnection;\r\n\r\nuse tera::Tera;\r\nuse tutorial::{\r\n    controller::{admin, front},\r\n    utils,\r\n};\r\n\r\nuse actix_web_httpauth::extractors::basic::{BasicAuth, Config};\r\nuse actix_web_httpauth::extractors::AuthenticationError;\r\nuse actix_web_httpauth::middleware::HttpAuthentication;\r\ntype DbPool = r2d2::Pool<ConnectionManager<MysqlConnection>>;\r\n\r\n#[actix_web::main]\r\nasync fn main() -> std::io::Result<()> {\r\n    std::env::set_var("RUST_LOG", "actix_web=info");\r\n    env_logger::init();\r\n    dotenv::dotenv().ok();\r\n\r\n    let pool = establish_connection();\r\n\r\n    HttpServer::new(move || {\r\n        let tera = Tera::new(concat!(env!("CARGO_MANIFEST_DIR"), "/templates/**/*")).unwrap();\r\n\r\n        let auth = HttpAuthentication::basic(basic_auth_validator);\r\n\r\n        let app = App::new();\r\n        app.data(tera)\r\n            .data(pool.clone())\r\n            .wrap(middleware::Logger::default()) // enable logger\r\n            // admin\r\n            .service(\r\n                web::scope("/admin/")\r\n                    .wrap(auth)\r\n                    .service(admin::index::index)\r\n                    .service(admin::types::list)\r\n                    .service(admin::types::type_add)\r\n                    .service(admin::types::type_edit)\r\n                    .service(admin::types::delete)\r\n                    .service(admin::article::list)\r\n                    .service(admin::article::add_page)\r\n                    .service(admin::article::add)\r\n                    .service(admin::article::delete)\r\n                    .service(admin::article::edit)\r\n                    .service(admin::article::upload)\r\n                    .service(admin::article::get_all_by_tid)\r\n                    .service(admin::article::edit_page)\r\n            )\r\n            // front\r\n            .service(front::index::index)\r\n            .service(front::index::article)\r\n            .service(front::index::goto_first_tutorial)\r\n            // file\r\n            .service(afs::Files::new("/", "./static/").show_files_listing())\r\n            .service(web::scope("").wrap(utils::errors::error_handlers()))\r\n    })\r\n    .bind("127.0.0.1:80")?\r\n    .run()\r\n    .await\r\n}\r\n\r\nfn establish_connection() -> DbPool {\r\n    let db_url = std::env::var("DATABASE_URL").expect("DATABASE_URL");\r\n    let manager = ConnectionManager::<MysqlConnection>::new(db_url);\r\n    r2d2::Pool::builder()\r\n        .build(manager)\r\n        .expect("Failed to create pool.")\r\n}\r\n\r\nasync fn basic_auth_validator(\r\n    req: ServiceRequest,\r\n    credentials: BasicAuth,\r\n) -> Result<ServiceRequest, Error> {\r\n    let config = req\r\n        .app_data::<Config>()\r\n        .map(|data| data.clone())\r\n        .unwrap_or_else(Default::default);\r\n    match validate_credentials(\r\n        credentials.user_id(),\r\n        credentials.password().unwrap().trim(),\r\n    ) {\r\n        Ok(res) => {\r\n            if res == true {\r\n                Ok(req)\r\n            } else {\r\n                Err(AuthenticationError::from(config).into())\r\n            }\r\n        }\r\n        Err(_) => Err(AuthenticationError::from(config).into()),\r\n    }\r\n}\r\n\r\nfn validate_credentials(user_id: &str, user_password: &str) -> Result<bool, std::io::Error> {\r\n    let name = std::env::var("ADMIN").expect("ADMIN");\r\n    let pass = std::env::var("PASSWORD").expect("PASSWORD");\r\n\r\n    println!("name:{}, pass:{}", name, pass);\r\n\r\n    if user_id.eq(&name) && user_password.eq(&pass) {\r\n        return Ok(true);\r\n    }\r\n    return Err(std::io::Error::new(\r\n        std::io::ErrorKind::Other,\r\n        "Authentication failed!",\r\n    ));\r\n}\r\n\r\n```', 0, '2020-12-29 14:59:54', '2020-12-29 14:59:54'),
('e9e177be446647739c9bb8080bdb7ad4', 'a7e45e1c0cdf4f1aa2e204d8ed71e9e5', '8f3bcabf732c47bf98f840c2fb135bc6', 'c语言环境搭建', 10, 'diesssssssssssssssssssssssssssss', 'keyyyyyyyyyyyyyyyyy', 'C语言中变bai量遵循“先定义后du使用”的原则：\r\n1、定义zhi变量dao的格式zhuan：数据类型 变量shu名;\r\n首先要强调的一点是：变量的定义是一条语句，每条语句都是以分号结尾的。故定义完变量，后面不要漏掉“；”分号。\r\n在变量定义中，“数据类型”表示想要存储什么类型的数据就定义什么类型的变量。\r\n如想要存储整数就定义成 int 型；想要存储小数就定义成 float 型或 double 型；想要存储字符就定义成 char 型等等。\r\n“变量名”就是你想给这个变量起个什么名字，通常都是用字母、数字与下划线组合而成。比如：\r\n“int i；double price；double goods_price2”等等。\r\n就表示定义了一个整型变量 i、小数型变量price、goods_price2；\r\n2、变量定义完成后，接下来就是使用变量，为变量赋值。\r\n将一个值放到一个变量中，这个动作叫“赋值”。通俗点讲，“给变量赋值”意思就是将一个值传给一个变量。\r\n赋值的格式是：\r\n变量名 = 要赋的值;', 0, '2020-12-28 03:40:07', '2020-12-28 03:40:07');

-- --------------------------------------------------------

--
-- 表的结构 `types`
--

CREATE TABLE IF NOT EXISTS `types` (
  `id` varchar(40) COLLATE utf8mb4_bin NOT NULL,
  `pid` varchar(40) COLLATE utf8mb4_bin DEFAULT '',
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL COMMENT '分类名称',
  `rank` int(11) DEFAULT '0' COMMENT '排序编号，数字越大越靠前，默认 0',
  `description` varchar(255) COLLATE utf8mb4_bin DEFAULT '' COMMENT '描述信息，用于seo',
  `keyword` varchar(255) COLLATE utf8mb4_bin DEFAULT '' COMMENT '关键词，用于seo',
  `icon` varchar(255) COLLATE utf8mb4_bin DEFAULT '' COMMENT '教程图标，教程列表的时候显示',
  `pic` varchar(255) COLLATE utf8mb4_bin DEFAULT '' COMMENT '教程图片，用于seo，分享的时候显示',
  `has_tutorial` tinyint(1) DEFAULT '0' COMMENT '分类下是否有教程，true=有',
  `hidden` tinyint(1) DEFAULT '1' COMMENT '是否隐藏，true隐藏',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='教程分类表';

--
-- 转存表中的数据 `types`
--

INSERT INTO `types` (`id`, `pid`, `name`, `rank`, `description`, `keyword`, `icon`, `pic`, `has_tutorial`, `hidden`, `created_at`) VALUES
('2854d469f1e24e66a19b6a2eaedf2733', '7cba3f42809f4e8ea3551744469ce612', 'http', 0, '', '', '', '', 0, 0, '2020-12-27 17:18:43'),
('7cba3f42809f4e8ea3551744469ce612', '', '网络协议', 0, '', '', '', '', 0, 0, '2020-12-27 17:18:22'),
('7fe3e46246e04c6f87fe41441471c571', 'a404058ffd6e45829d98d8179f5bff38', 'rust语言', 5, '', '', '/upload/9d812bca-6e13-4561-a006-87b8adf94673.jpg', '/upload/9264b2c1-b3a8-4830-8a48-3476f80d2ffc.png', 0, 0, '2020-12-27 16:44:06'),
('8f3bcabf732c47bf98f840c2fb135bc6', 'a404058ffd6e45829d98d8179f5bff38', 'c语言', 10, '', 'c语言', '', '', 0, 0, '2020-12-28 02:01:42'),
('a404058ffd6e45829d98d8179f5bff38', '', '编程', 1, '', '', '', '', 0, 0, '2020-12-27 16:24:03');

-- --------------------------------------------------------

--
-- 表的结构 `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` char(40) CHARACTER SET utf8 NOT NULL,
  `username` varchar(50) CHARACTER SET utf8 NOT NULL,
  `password` varchar(100) CHARACTER SET utf8 NOT NULL,
  `avatar` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `enable` tinyint(1) DEFAULT '1' COMMENT '是否被禁用，1=未禁用 0=禁用',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- 表的结构 `__diesel_schema_migrations`
--

CREATE TABLE IF NOT EXISTS `__diesel_schema_migrations` (
  `version` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `run_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- 转存表中的数据 `__diesel_schema_migrations`
--

INSERT INTO `__diesel_schema_migrations` (`version`, `run_on`) VALUES
('20201208015730', '2020-12-27 16:21:56'),
('20201208015746', '2020-12-27 16:21:57'),
('20201208015753', '2020-12-27 16:21:57');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tutorials`
--
ALTER TABLE `tutorials`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tutorials_ibfk_1` (`tid`);

--
-- Indexes for table `types`
--
ALTER TABLE `types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `__diesel_schema_migrations`
--
ALTER TABLE `__diesel_schema_migrations`
  ADD PRIMARY KEY (`version`);

--
-- 限制导出的表
--

--
-- 限制表 `tutorials`
--
ALTER TABLE `tutorials`
  ADD CONSTRAINT `tutorials_ibfk_1` FOREIGN KEY (`tid`) REFERENCES `types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
