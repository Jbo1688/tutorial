-- Your SQL goes here


CREATE TABLE IF NOT EXISTS `users` (
  `id` char(40) CHARACTER SET utf8 NOT NULL primary key,
  `username` varchar(50) CHARACTER SET utf8 NOT NULL,
  `password` varchar(100) CHARACTER SET utf8 NOT NULL,
  `avatar` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `enable` tinyint(1) DEFAULT '1' COMMENT '是否被禁用，1=未禁用 0=禁用',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
